(function() {
    "use strict"
    angular.module("magext")
        .controller("contentCtrl", contentCtrl)

    contentCtrl.$inject = ['$scope', '$rootScope', 'peaPages', '$state', '$stateParams', '$ionicSlideBoxDelegate', '$ionicScrollDelegate', '$timeout', 'peaLocalStorage', 'peaRequestData', 'peaDialog', 'peaVideo'];

    function contentCtrl($scope, $rootScope, peaPages, $state, $stateParams, $ionicSlideBoxDelegate, $ionicScrollDelegate, $timeout, peaLocalStorage, peaRequestData, peaDialog, peaVideo) {
        //params
        var parentPageId = $stateParams.nodeId;
        // var type = $stateParams.type;
        //declear contentType scope variable to check in nav title if normal content or presentation content
        // $scope.$parent.contentType = type;
        $scope.enableContentButton;
        $scope.enableVideoButton;
        $scope.onGallerySwipe = onGallerySwipe;
        $scope.isSingleColumn = false;
        $scope.showPager = false;
        $scope.showMainSliderPager = false;
        $scope.showBottomArrow = false;
        $scope.showTopArrow = false;
        $scope.fullWidth;
        $scope.selectedPageId;
        $scope.setSelected
        $scope.allData;
        $scope.sliderIndex = 0;

        //functions

        $scope.initContentPage = initContentPage;
        $scope.openGalleryModal = openGalleryModal;
        $scope.hideFirstColumn = hideFirstColumn;
        $scope.slideBoxSwipe = slideBoxSwipe;
        $scope.setColumnClass = setColumnClass;
        $scope.slideHasChanged = slideHasChanged;
        $scope.imageHeightHandler = imageHeightHandler;
        $scope.pageButtonsHandler = pageButtonsHandler;
        $scope.BtnsFullScreenHandler = BtnsFullScreenHandler;
        $scope.containerFullScreenHandler = containerFullScreenHandler;
        $scope.onBottom = onBottom;
        $scope.onTop = onTop;
        $scope.setSelectedBottomItem = setSelectedBottomItem;
        $scope.openVideoModal = openVideoModal;



        function initContentPage() {
            peaRequestData.getAllData()
                .then(function(alldata) {
                    $scope.alldata = alldata.data.en[0];
                    var __page = peaPages.getPageById(parentPageId, $scope.alldata).navigation;
                    $scope.pageList = _.reject(__page, function(page) {
                        return is.empty(page.content);
                    });

                    $scope.$watch('selectedPageId', function(pageId) {
                        var firstIteration = true;
                        var __pageId;
                        _.forEach($scope.pageList, function(page) {
                            if (firstIteration && is.undefined(pageId)) {
                                __pageId = page.id;
                                $scope.selectedPageId = page.id;
                            } else {
                                __pageId = pageId;
                            }

                            if (page.id === __pageId) {
                                $scope.content = page.content;
                            }
                            firstIteration = false;
                        });
                        if (is.not.undefined($scope.content)) {
                            $scope.$emit("onContentDataIsReady", $scope.content);
                        }
                    });
                });
        }

        $scope.$on("onContentDataIsReady", function(content) {
            if ($scope.content && $scope.content.length !== 0) {
                $scope.$watch('sliderIndex', function() {
                    // index validation[must number && must less than content length]
                    if (isNaN($scope.sliderIndex) || $scope.sliderIndex >= $scope.content.length)
                        $scope.sliderIndex = 0;
                    $scope.$parent.currentContentId = $scope.content[$scope.sliderIndex].id
                    _.each($scope.content[$scope.sliderIndex].cols, function(item) {
                        $scope.showPager = (item.type == 'gallery' && item.gallery_imgs.length > 1) ? true : false
                    });
                });
                //show main pager
                if ($scope.content.length > 1)
                    $scope.showMainSliderPager = true;
            }
            //there are no contents in this page
            else {
                $scope.$parent.currentContentId = 0;
            }
        })


        function openVideoModal(videosLinks) {

        	//the below snippet is for test only.
            // var videosLinks = [{
            //     "id": "6",
            //     "title": "VAME Image video",
            //     "sub_title": "",
            //     "auther": "Lukas Leitner",
            //     "src": "http://api.voestalpineapp.sps-digital.com/cdn/uploads/videos/mp414697737289010287949.mp4",
            //     "thumb": "http://api.voestalpineapp.sps-digital.com/cdn/uploads/png14697736832796536534.png",
            //     "create_date": "2016-07-29",
            //     "background_img": ""
            // }];

            if (videosLinks && videosLinks.length !== 0) {
            $scope.playBtn = true;
            peaVideo.init($scope, videosLinks).then(function(modal) {
                modal.show();
            });
            }
        }


        //change slider index variable from the current page index
        function slideHasChanged(index) {
            $scope.sliderIndex = index;
        }

        //prevent swiping the slide box if the gallery swiped
        function onGallerySwipe(e) {
            $ionicSlideBoxDelegate.$getByHandle('mainSlider').enableSlide(false);
            $scope.$watch("sliderIndex", function() {
                if (!GalleryIsSingleImage($scope.sliderIndex)) {
                    e.stopPropagation();
                }
            });
        }
        //enable slide box swiping 
        function slideBoxSwipe(e) {
            _.each($scope.content[$scope.sliderIndex].cols, function(item, i) {
                if (item.type == 'gallery' && item.gallery_imgs.length > 1) {
                    $ionicSlideBoxDelegate.$getByHandle('mainSlider').enableSlide(false);
                } else {
                    $ionicSlideBoxDelegate.$getByHandle('mainSlider').enableSlide(true);
                }
            });

            $ionicSlideBoxDelegate.enableSlide(true);
            //when swipe close bottom menu
            // $scope.toggleBottomMenu(true);
        }
        //fill the entire page if the slider contain one column.
        function setColumnClass(col) {
            if (isOneCol(col.length)) {
                return 'col-md-12 full-height';
            } else if (!isOneCol(col.length)) {
                if (is.even(col.index)) {
                    return 'col-md-5 two-col-page-height col-1'
                } else if (is.odd(col.index)) {
                    return 'col-md-7 two-col-page-height col-2'
                }
            }
        }
        //hiding the first column
        function hideFirstColumn(colLength) {
            if (colLength > 1) {
                return false;
            } else {
                return true;
            }
        }

        //open gallery modal on image click.
        function openGalleryModal(index) {
            var dataSource = new Array();
            _.each($scope.content[$scope.sliderIndex].cols, function(item) {
                if (item.type == 'gallery') {
                    dataSource = item.gallery_imgs;
                } else if (item.type == 'img') {
                    var imageObj = {}
                    imageObj.image = item.img
                    dataSource.push(imageObj)
                }
            });
            peaDialog.showGalleryModal($scope, dataSource, index);
        }

        //this function will fired if the scroll reached bottom and then hide the arrow
        function onBottom() {
            $scope.showTopArrow = true;
            $scope.showBottomArrow = false;
        }

        function onTop() {
            $scope.showTopArrow = false;
            $scope.showBottomArrow = true;
        }

        $scope.$on('ngRepeatFinished', function() {
            showArrow();
            $ionicSlideBoxDelegate.update();
        });

        //change image height if the page has one column
        function imageHeightHandler(colLength) {
            if (isOneCol(colLength)) {
                return "full-image-height"
            }
        }

        //display button on the right of the content if the content has one column.
        function videoBtnHandler(colLength) {
            if (isOneCol(colLength)) {
                return 'button-align-right';
            } else {
                return;
            }
        }

        function pageButtonsHandler(colLength) {
            var videoButton = $('.search-button');
            if (isOneCol(colLength)) {
                return "button-align-right";
            } else {
                return "col-md-offset-5";
            }
        }


        //change button position in full screen mode
        function BtnsFullScreenHandler(colLength) {
            if (isOneCol(colLength)) {
                return "buttons-container-fullscreen"
            } else {
                return "buttons-container"
            }
        }

        //change height of the page in full screen mode
        function containerFullScreenHandler(colLength) {
            if (isOneCol(colLength)) {
                return "slide-body-fullScreen"
            } else {
                return "slide-body"
            }
        }

        //return true if the gallery has only one image
        function GalleryIsSingleImage(index) {
            var isSingleImage;
            _.each($scope.content[index].cols, function(item) {
                if (item.type == 'gallery' && item.gallery_imgs.length == 1)
                    isSingleImage = true;

            });
            return isSingleImage;
        }

        function showArrow() {
            $scope.$watch('sliderIndex', function() {
                var textHeight = $('#text-container').height();
                var containerHeight = $('#col-0').height();
                if (textHeight > containerHeight) {
                    $scope.showBottomArrow = true;
                    textHeight = 0;
                }
            });
        }


        function setSelectedBottomItem(itemId) {
            $scope.selectedPageId = itemId;
        }


        //check the columns number
        function isOneCol(cols) {
            if (cols == 1) {
                return true;
            } else {
                return false
            }
        }

    }
})();
