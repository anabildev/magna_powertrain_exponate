(function(){
    'use strict';
    angular.module('magext')
    .controller('mainCtrl' , mainCtrl)

    mainCtrl.$injector = ['$scope', '$state', 'peaRequestData'];

    function mainCtrl($scope, $state, peaRequestData){
        //define controller variables
        $scope.list;
        
        //define view functions
        $scope.onItemClick = onItemClick;
        
        peaRequestData.getAllData()
        .then(function(pages){
            $scope.list = pages.data.en[0].navigation;
        })


        //scope functions
        function onItemClick(event, itemId){
            var _element = $(event.target);
            _element.toggleClass("active-item");
			$('.list-item').not(_element).removeClass('active-item');
            // console.log('itemID', itemId);
            $state.go('content', {'nodeId':itemId});
        }


    }

})();