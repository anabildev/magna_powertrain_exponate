
(function () {
	"use strict";
	angular.module("magext")
		.filter('length', function () {
			return function (item, length) {
				if(item !== undefined) {
					//if text length overflow cut length
					if(item.length > length)
						item = item.substr(0,length) + '..'
				}
				return item
			};
		});
})();