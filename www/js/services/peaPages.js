(function () {
	"use strict";
	angular.module("magext")
		.factory("peaPages", vsaPages)

	vsaPages.$inject = ['$timeout', '$state'];

	function vsaPages($timeout, $state) {
		var returnedData = {};

		//take json data and return rootNode object
		function createNode(data) {
			var tree = new TreeModel({
				childrenPropertyName:'navigation',
			});
			var rootNode = tree.parse(data);
			return rootNode;
		}
		//take pagID and data and return node
		function getNodeById (pageId, data) {
			var rootNode = createNode(data)
			var node = rootNode.first(function (node) {
				return node.model.id == pageId;
			});
			return node;
		}
		//get array of nodes return array of pages
		function getPagesFromNodes (nodes) {
			var __pages = _.map(nodes, function(pathItem){ 
				return pathItem.model; 
			});
			return __pages;
		}

		//[*]take page id and return page object data
		function getPageById (pageId, data) {
			var node = getNodeById(pageId, data);
			return node ? node.model : false;
		}
		//[*]take page id and return page path
		function getPathById (pageId, data) {
			var node = getNodeById(pageId, data);
			if (!node)
				return false;
			//path of nodes
			var nodePath = node.getPath();
			//if path invalid or empty return false
			if(!nodePath || nodePath.length == 0) {
				return false;
			}
			else {
				//transfer node path to pages path
				var pagePath = getPagesFromNodes(nodePath);
				return pagePath;
			}
		}

		returnedData.createNode 	= createNode;
		returnedData.getPagesFromNodes = getPagesFromNodes;
		returnedData.getPageById 	= getPageById;
		returnedData.getPathById 	= getPathById;
		return returnedData;
	}

})();