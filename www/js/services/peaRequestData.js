(function() {
    'use strict';
    angular.module('magext')
        .service('peaRequestData', maeRequestData);

    maeRequestData.$injector = ['$resource', '$q'];

    function maeRequestData($resource, $q) {
        var returnedData = {};
        var token = 'letmein';
        var serverPath = "http://api.voestalpineapp.sps-digital.com/index.php/";
        var testPath = "http://10.1.67.15/magna-backend/index.php/";

        function getAllData() {
            return $resource(serverPath + '/Page/listAllPagesStructured?token=' + token).get().$promise;
        }

        // function get(param, id) {
        //     var _returnedData;
        //     var getAllData = this.getAllData();
        //     return $q(function(resolve, reject) {
        //         if (param == 'navigation') {
        //             getAllData.then(function(data) {
        //                 console.log('navigation', data.data.en[0].navigation[0].navigation);
        //                 resolve(data.data.en[0].navigation);
        //             });
        //         } else if (param == 'contents') {
        //             getAllData.then(function(data) {
        //                 var allContents = data.data.en[0].navigation[0].navigation;
        //                 _.forEach(allContents, function(page) {
        //                     if (page.id == id) {
        //                         console.log('page content of id ==> ' + id, page.content);
        //                         resolve(page.content);
        //                     }
        //                 });
        //             })
        //         } else if (param == 'pages') {
        //             getAllData.then(function(data) {
        //                 var allPages = data.data.en[0].navigation[0].navigation;
        //                 resolve(allPages);

        //             })
        //         }
        //     });
        // }


        returnedData.getAllData = getAllData;
        return returnedData;
    }

})();
