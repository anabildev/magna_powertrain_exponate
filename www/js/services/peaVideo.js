(function () {
	"use strict";
	angular.module("magext")
		.service("peaVideo", vsaVideo)

	vsaVideo.$inject = ['$ionicModal', '$rootScope', '$sce'];

	function vsaVideo($ionicModal, $rootScope, $sce) {

	    var animation = ($rootScope.windows) ? null : 'slide-in-up';

		this.init = function($scope, videosLinks) {
			var videoLength = videosLinks.length;
			var promise;
			$scope = $scope || $rootScope.$new();

			$scope.showVideo = false;
			$scope.videoPlayerAPI = null;
			//define function take [videoLink] link of video to preview, [autoplay] true one video, false if grid, [hideVideoModal] isset true if video from grid
			$scope.previewVideo = function($event, videoLink, autoplay, hideVideoModal){
				if($event !== false) {
					$event.stopPropagation();
				}
				if(hideVideoModal) {
					$scope.hideVideoModal = true;
				}
				$scope.showVideo = true;
				$scope.config = {
					sources: [
					{src: $sce.trustAsResourceUrl(videoLink), type: "video/mp4"}
					],
					theme: "lib/modules/videogular-themes-default/videogular.css",
					autoPlay: autoplay
				}
			}

			//if there are exactly one video
			if(videoLength == 1) {
				//preview videogular
				$scope.previewVideo(false, videosLinks[0].src, true);
			}
			//if there are more than one video
			else {
				$scope.showVideo = false;
				$scope.videosLinks = videosLinks;
			}

			promise = $ionicModal.fromTemplateUrl('templates/modals/video.html', {
				scope: $scope,
				animation: animation
			}).then(function(modal) {
				$scope.modal = modal;
				return modal;
			});
			
			//close the video[press on Done Button]
			$scope.closeVideo = function(){
				$scope.playBtn = false;
				if($scope.showVideo){
					//if there are one video .. close whole modal 
					if(videoLength == 1) {
						$scope.modal.hide();
						$scope.modal.remove();
					}
					$scope.showVideo = false;

					if ($scope.videoPlayerAPI && $scope.videoPlayerAPI != null) {
						$scope.videoPlayerAPI.stop();
						$scope.config = null;
					}
				}
			}
			// Close the modal
			$scope.closeModal = function() {
				$scope.playBtn = false;
				$scope.modal.hide();
				$scope.modal.remove();
			}
			//close the video
			$scope.stopPropagation = function($event){
				$event.stopPropagation();
			}

			$scope.onPlayerReady = function onPlayerReady (API) {
				$scope.videoPlayerAPI = API;
			}

			$scope.$on('$destroy', function() {
				$scope.modal.remove();
			});

			return promise;
		}
	}

})();