(function() {
    "use strict";
    angular.module("magext")
        .factory("peaDialog", vsaDialog)

    vsaDialog.$inject = ['ngDialog', '$rootScope'];

    function vsaDialog(ngDialog, $rootScope) {
        var isCordovaApp = $rootScope.isCordovaApp;
        var returnedData = {};

        //disable animation on windows device related to animation issue.
        var animation = ($rootScope.windows) ? true : false;

        returnedData.showAlert = function($scope, alertTxt, showClose) {
            $scope.alertTxt = alertTxt;
            var alert = ngDialog.open({
                    scope: $scope,
                    showClose: showClose,
                    template: 'templates/modals/alert.html',
                    className: 'ngdialog-theme-plain delete-modal ',
                    name: "alert confirmDelete",
                    closeByNavigation: true,
                    disableAnimation: animation
                })
                //return promise which will resolve when the dialog is closed
            return alert.closePromise;

        }

        returnedData.showGalleryModal = function($scope, dataSource, index) {
            $scope.dataSource = dataSource;

            var gallery = ngDialog.open({
                id: 'galleryModal',
                scope: $scope,
                closeByEscape: true,
                disableAnimation: animation,
                template: "templates/modals/gallery-modal.html",
                className: 'ngdialog-theme-default gallery-modal-main',
                controller: galleryCtrl
            });


            function galleryCtrl($scope, $ionicSlideBoxDelegate, $timeout) {
                $scope.$parent.fullScreenImage = false;
                $scope.galleryIndex = 0;
                $scope.fillPage = function() {
                    $scope.$parent.fullScreenImage = true;
                }
                var showPager
                if (dataSource.length == 1) {
                    showPager = false;
                } else {
                    showPager = true;
                }

                $scope.options = {
                    loop: false,
                    pager: true,
                    pagination: true,
                    initialSlide: index,
                    onSlideChangeEnd: function(swiper) {
                        $scope.galleryIndex = swiper.activeIndex;
                    }
                }
                $scope.data = {};
                $scope.$watch('data.slider', function(newValue, oldValue) {
                    $scope.slider = $scope.data.slider;
                });
                $timeout(function() {
                    $scope.slider.slideTo(index);
                }, 500);
                $scope.$watch('fullScreenImage', function(newValue) {
                    if (newValue) {
                        returnedData.showGalleryModalFullScreen($scope, dataSource[$scope.galleryIndex]);
                        $scope.$parent.fullScreenImage = false;
                    }
                });
            }


            return gallery;
        }

        returnedData.showGalleryModalFullScreen = function($scope, selectedImage) {
            var gallery = ngDialog.open({
                scope: $scope,
                closeByEscape: true,
                disableAnimation: animation,
                template: "templates/modals/full-screen-image.html",
                className: 'ngdialog-theme-default gallery-modal-main-fullscreen',
                controller: function($scope, $ionicSlideBoxDelegate, $timeout) {
                    $timeout(function() {
                        $scope.image = selectedImage.image;
                    }, 200);
                }
            });
            return gallery;
        }

        //close All dialoges
        returnedData.close = function(id) {
            ngDialog.close(id);
        }

        //close All dialoges
        returnedData.closeAll = function() {
            ngDialog.closeAll();
        }

        return returnedData;
    }

})();
