(function () {
	"use strict";
	angular.module("magext")
		.factory("peaLocalStorage", vsaLocalStorage)

	vsaLocalStorage.$inject = [];

	function vsaLocalStorage() {
		var returnedData = {};

		returnedData.get 	= get;
		returnedData.set 	= set;
		returnedData.remove = remove;

		//get data from localsorage
		function get (name, isObject) {

			var data = localStorage.getItem(name);

			//id data is null or undifiend or empty return false
			if(!data)
				data = false;
			//if is there data parse it to JSON object
			else {
				if(isObject)
					data = JSON.parse(data);
			}

			return data;
		}
		//set data into localsorage
		function set (name, data) {
			var isDataSeted = true;

			if(typeof name !== 'string')
				isDataSeted = false;
			//if data object parse it to string
			if(typeof data == 'object')
				data = JSON.stringify(data);

			localStorage.setItem(name, data);
			return isDataSeted;
		}
		
		function remove (name) {
			localStorage.removeItem(name);
		}
		
		return returnedData;
	}

})();