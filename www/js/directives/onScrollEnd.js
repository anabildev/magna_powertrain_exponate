(function(){
    'use strict';
    angular.module('magext')
    .directive('onScrollEnd' , onScrollEnd);

    function onScrollEnd(){
       return {
           restrict:'A',
           scope:{
               onTop:'&',
               onBottom:'&'
           },
           link:function(scope, element, attr){
               element.scroll(function(){
                   var raw = element[0];
                    if (raw.scrollTop + raw.offsetHeight == raw.scrollHeight) {
                            scope.onBottom();
                            scope.$apply(attr.onBottom);
                    }else{
                            scope.onTop();
                            scope.$apply(attr.onTop);
                    }
               });
           }
       } 
    }
})();