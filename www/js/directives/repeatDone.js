(function(){
	'use strict';

	angular.module('magext')
    .directive('repeatDone', function ($timeout) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attr) {
	            if (scope.$last === true) {
	                    scope.$emit('ngRepeatFinished');
	            }
	        }
	    }
	});

})();
